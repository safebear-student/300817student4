package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 30/08/2017.
 */
public class Test01_Login extends BaseTest {@Test
    public void testLogIn () {
    //Step 1 Confirm we're on the Welcome Page
    assertTrue(welcomePage.checkCorrectPage());

    //Step 2 Click to Log In
    assertTrue(welcomePage.clickOnLogin(this.loginPage));

    //Step 3 Log in with valid credentials
    assertTrue(loginPage.login(this.userPage,"testuser","testing"));
}
}
