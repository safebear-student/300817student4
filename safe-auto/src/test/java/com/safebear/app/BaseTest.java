package com.safebear.app;

import com.safebear.app.pages.FramesPage;
import com.safebear.app.pages.LoginPage;
import com.safebear.app.pages.UserPage;
import com.safebear.app.pages.WelcomePage;
import com.safebear.app.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 30/08/2017.
 */
public class BaseTest {
    WebDriver driver;
    Utils utility;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;
    FramesPage framesPage;


@Before
    public void setUp (){
    driver = new ChromeDriver();
    utility = new Utils();
    welcomePage = new WelcomePage(driver);
    loginPage = new LoginPage(driver);
    userPage = new UserPage(driver);
    framesPage = new FramesPage(driver);
    assertTrue (utility.navigateToWebsite(driver));
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    driver.manage().window().maximize();


}
@After
    public void tearDown (){
    try {
        TimeUnit.SECONDS.sleep(4);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    driver.quit();

}
}
